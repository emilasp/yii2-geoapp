<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160406_105508_AddTableStreet extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('geo_street_data', [
            'id' => $this->primaryKey(11),

            'name' => $this->string(40)->notNull(),
            'socr' => $this->string(10),

            'code'         => $this->string(16)->notNull(),
            'code_subject' => $this->string(2)->notNull(),
            'code_region'  => $this->string(3)->notNull(),
            'code_city'    => $this->string(3)->notNull(),
            'code_town'    => $this->string(3)->notNull(),
            'code_full'    => $this->string(17)->notNull(),

            'actual' => $this->string(2)->notNull(),

            'index'         => $this->string(6),
            'gninmb'        => $this->string(4),
            'uno'           => $this->string(4),
            'ocatd'         => $this->string(11),

            'parent_id'     => $this->integer(11),
            'parent_code'   => $this->string(13),
            'parent_string' => $this->string(250),
        ], $this->tableOptions);

        $this->createIndex('idx_geo_street_data_index', 'geo_street_data', 'index');
        $this->createIndex('idx_geo_street_data_code', 'geo_street_data', 'code');
        $this->createIndex('idx_geo_street_data_code_town', 'geo_street_data', 'code_town');
        $this->createIndex('idx_geo_street_data_code_city', 'geo_street_data', 'code_city');
        $this->createIndex('idx_geo_street_data_code_region', 'geo_street_data', 'code_region');
        $this->createIndex('idx_geo_street_data_code_subject', 'geo_street_data', 'code_subject');
        $this->createIndex('idx_geo_street_data_actual', 'geo_street_data', 'actual');

        $this->fillStreetTable();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('geo_street_data');

        $this->afterMigrate();
    }


    private function fillStreetTable()
    {
        echo 'Fill street table..'.PHP_EOL;
        $sql = <<<SQL
INSERT INTO  geo_street_data("name", socr,code, code_subject, code_region, code_city, code_town, code_full, actual,
                            "index", gninmb, uno, ocatd, parent_code, parent_string)
  SELECT
    "name",
    socr,

    substr(code,1,13) code,        -- СС РРР ГГГ ППП Собственно код без флага актуальности
    substr(code,1,2) code_subject,  -- СС Сегмент субъект РФ
    substr(code,3,3) code_region,     -- РРР Сегмент район
    substr(code,6,3) code_city,       -- ГГГ Сегмент город
    substr(code,9,3) code_town,       -- ППП Сегмент населенный пуект в городе
    code,

    substr(code,14,2) actual , -- АА флаг актуальности

    "index",
    gninmb,
    uno,
    ocatd,

    -- Предок
    CASE CASE WHEN substr(code,12,3)<>'000' THEN '5'
         WHEN substr(code,9,3)<>'000' THEN '4'
         WHEN substr(code,6,3)<>'000' THEN '3'
         WHEN substr(code,3,3)<>'000' THEN '2'
         ELSE '1'
         END
    WHEN '1' THEN NULL
    WHEN '2' THEN RPad(substr(code,1,2),13,'0')
    WHEN '3' THEN RPad(substr(code,1,5),13,'0')
    WHEN '4' THEN RPad(substr(code,1,8),13,'0')
    WHEN '5' THEN RPad(substr(code,1,11),13,'0')
    END  parent_code,

    0 as parent_string

FROM street;
SQL;

        $this->db->createCommand($sql)->execute();
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
