<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160406_064222_FillKladrTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
       $this->addFunctionInStr();
       $this->fillKladrTable();
       $this->fillKladrTableParentId();

       $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('files_file');

        $this->afterMigrate();
    }

    private function fillKladrTableParentId()
    {
        echo 'Fill parent id..'.PHP_EOL;
        $sql = <<<SQL
UPDATE geo_kladr_data data
SET parent_id = data2.id,
    parent_string=
     CASE  WHEN data2.socr='округ' OR data2.socr='км' OR data2.socr='обл' OR data2.socr='р-н' THEN CAST (data2.name || ' ' || data2.socr AS VARCHAR (50))
    ELSE CAST (data2.socr || ' ' || data2.name AS VARCHAR (50))
    END
FROM geo_kladr_data data2
WHERE data2.code=data.parent_code
AND data.parent_code Is NOT NULL
AND data.level>1;
SQL;
        $this->db->createCommand('CREATE INDEX idx_geo_kladr_data_parent_code ON geo_kladr_data(parent_code);')->execute();
        $this->db->createCommand($sql)->execute();
        $this->db->createCommand('DROP INDEX  idx_geo_kladr_data_parent_code;')->execute();
    }

    private function addFunctionInStr()
    {
        echo 'Add function InStr..'.PHP_EOL;
        $sql = <<<SQL
create or replace function instr(str text, sub text, startpos int = 1, occurrence int = 1)
  returns int language plpgsql
as $$
declare
  tail text;
  shift int;
  pos int;
  i int;
begin
  shift:= 0;
  if startpos = 0 or occurrence <= 0 then
    return 0;
  end if;
  if startpos < 0 then
    str:= reverse(str);
    sub:= reverse(sub);
    pos:= -startpos;
  else
    pos:= startpos;
  end if;
  for i in 1..occurrence loop
    shift:= shift+ pos;
    tail:= substr(str, shift);
    pos:= strpos(tail, sub);
    if pos = 0 then
      return 0;
    end if;
  end loop;
  if startpos > 0 then
    return pos+ shift- 1;
  else
    return length(str)- pos- shift+ 1;
  end if;
end $$;
SQL;
        $this->db->createCommand($sql)->execute();
    }

    private function fillKladrTable()
    {
        echo 'Fill kladr table..'.PHP_EOL;
        $sql = <<<SQL
INSERT INTO  geo_kladr_data("name", name_type, name_comment, name_full, socr,code, code_subject, code_region, code_city, code_town, code_full,
                            actual, "index", gninmb, uno, ocatd, "level", count, parent_code, parent_string, status)
  SELECT
    -- "name",
    -- текст до '('  - собственно наименование
    CASE WHEN InStr("name",'(',1) > 0 THEN
      RTrim(SubStr("name",1,InStr("name",'(',1)-1))
    ELSE "name" END  "name",

    CASE WHEN InStr (translate( name
    ,'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯ1234567890N'
    , RPad('*',Length('АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯ1234567890N'),'*') )
    ,'*',1 ) > 0 THEN
      Trim(SubStr("name",1,InStr (translate( name
      ,'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯ1234567890N'
      , RPad('*',Length('АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯ1234567890N'),'*') )
      ,'*',1 ) - 1
           ))
    ELSE NULL END  "name_type",


    --name,
    -- текст начиная с '('...  - комментарий, разрешает неоднозначность наименования в пределах предка.
    CASE WHEN InStr("name",'(',1) > 0 THEN
      LTrim(SubStr("name", InStr("name",'(',1)))
    ELSE NULL END  name_comment,

    CASE  WHEN socr='округ' OR socr='км' OR socr='обл' OR socr='р-н' THEN CAST (name || ' ' || socr AS VARCHAR (50))
    ELSE CAST (socr || ' ' || name AS VARCHAR (50))
    END "name",

    socr,

    substr(code,1,11) code,        -- СС РРР ГГГ ППП Собственно код без флага актуальности
    substr(code,1,2) code_subject,  -- СС Сегмент субъект РФ
    substr(code,3,3) code_region,     -- РРР Сегмент район
    substr(code,6,3) code_city,       -- ГГГ Сегмент город
    substr(code,9,3) code_town,       -- ППП Сегмент населенный пуект в городе
    code,

    substr(code,12,2) actual , -- АА флаг актуальности

    "index",
    gninmb,
    uno,
    ocatd,

    -- Уровень
    CASE  WHEN substr(code,9,3)<>'000' THEN 4
    WHEN substr(code,6,3)<>'000' THEN 3
    WHEN substr(code,3,3)<>'000' THEN 2
    ELSE 1
    END as "level",


    0 as count,

    -- Предок
    CASE CASE WHEN substr(code,9,3)<>'000' THEN '4'
         WHEN substr(code,6,3)<>'000' THEN '3'
         WHEN substr(code,3,3)<>'000' THEN '2'
         ELSE '1'
         END
    WHEN '1' THEN NULL
    WHEN '2' THEN RPad(substr(code,1,2),11,'0')
    WHEN '3' THEN RPad(substr(code,1,5),11,'0')
    WHEN '4' THEN RPad(substr(code,1,8),11,'0')
    END  parent_code,

    0 as parent_string,

    to_number(status, '9')

FROM kladr;
SQL;

        $this->db->createCommand($sql)->execute();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
