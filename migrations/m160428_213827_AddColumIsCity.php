<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160428_213827_AddColumIsCity extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->addColumn('geo_kladr_data', 'is_city', 'int NULL DEFAULT 0');

        $sql = <<<SQL
        UPDATE geo_kladr_data SET is_city=1 WHERE socr NOT IN ('р-н','Респ','край','обл','Аобл','АО');
SQL;
        $this->db->createCommand($sql)->execute();


        $this->createIndex('geo_kladr_data_is_city_idx', 'geo_kladr_data', ['is_city']);

        $this->db->schema->refresh();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropColumn('geo_kladr_data', 'is_city');

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
