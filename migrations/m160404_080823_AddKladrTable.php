<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/*INSERT INTO  x_KLADR
SELECT
  CODE, SOCR, "NAME", "INDEX", GNINMB,UNO,OCATD,STATUS,
  substr(CODE,1,11) xk_Code,        -- СС РРР ГГГ ППП Собственно код без флага актуальности
  substr(CODE,1,2) xk_1RFSubject_Code,  -- СС Сегмент субъект РФ
  substr(CODE,3,3) xk_2Region_Code,     -- РРР Сегмент район
  substr(CODE,6,3) xk_3City_Code,       -- ГГГ Сегмент город
  substr(CODE,9,3) xk_4Town_Code,       -- ППП Сегмент населенный пуект в городе
  substr(CODE,12,2) xk_Actual_Flag , -- АА флаг актуальности
-- Уровень
  CASE  WHEN substr(code,9,3)<>'000' THEN '4'
        WHEN substr(code,6,3)<>'000' THEN '3'
        WHEN substr(code,3,3)<>'000' THEN '2'
        ELSE '1'
  END xk_Level,
  -- Предок
  CASE CASE WHEN substr(code,9,3)<>'000' THEN '4'
            WHEN substr(code,6,3)<>'000' THEN '3'
            WHEN substr(code,3,3)<>'000' THEN '2'
            ELSE '1'
       END
       WHEN '1' THEN NULL
       WHEN '2' THEN RPad(substr(CODE,1,2),11,'0')
       WHEN '3' THEN RPad(substr(CODE,1,5),11,'0')
       WHEN '4' THEN RPad(substr(CODE,1,8),11,'0')
  END  xk_Parent_Code,
  -- текст до первой заглавной буквы, знака N или цифры - своего рода список под типов ( x_Socr_In_Names)
  Trim(SubStr("NAME",1,InStr (translate( name
              ,'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯ1234567890N'
              , RPad('*',Length('АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯ1234567890N'),'*') )
          ,'*',1 ) - 1
  )) xk_Name_Type,
  -- текст до '('  - собственно наименование
  CASE WHEN InStr("NAME",'(',1) > 0 THEN
       RTrim(SubStr("NAME",1,InStr("NAME",'(',1)-1))
       ELSE "NAME" END  xk_Name,
  -- текст начиная с '('...  - комментарий, разрешает неоднозначность наименования в пределах предка.
  CASE WHEN InStr("NAME",'(',1) > 0 THEN
       LTrim(SubStr("NAME", InStr("NAME",'(',1)))
       ELSE NULL END  xk_Name_Comm*/

class m160404_080823_AddKladrTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('geo_kladr_data', [
            'id' => $this->primaryKey(11),

            'name'         => $this->string(40)->notNull(),
            'name_type'    => $this->string(40),
            'name_comment' => $this->string(40),
            'name_full'    => $this->string(60)->notNull(),

            'socr' => $this->string(10),

            'code'         => $this->string(12)->notNull(),
            'code_subject' => $this->string(2)->notNull(),
            'code_region'  => $this->string(3)->notNull(),
            'code_city'    => $this->string(3)->notNull(),
            'code_town'    => $this->string(3)->notNull(),
            'code_full'    => $this->string(13)->notNull(),

            'actual' => $this->string(2)->notNull(),

            'index'         => $this->string(6),
            'gninmb'        => $this->string(4),
            'uno'           => $this->string(4),
            'ocatd'         => $this->string(11),
            'level'         => $this->integer(1),
            'count'         => $this->integer(),
            'parent_id'     => $this->integer(11),
            'parent_code'   => $this->string(13),
            'parent_string' => $this->string(250),
            'status'        => $this->integer(1),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_geo_kladr_data_parent_id',
            'geo_kladr_data',
            'parent_id',
            'geo_kladr_data',
            'id'
        );

        //$this->fillLevels();


        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('geo_kladr_data');

        $this->afterMigrate();
    }

    /**
     * В таблице kladr все построено более менее иерархично.
     * aa0bb0cc0xxxx
     *
     * где aa -регион
     * bb - район (область)
     * сс - города
     * хххх - нас. пункты.
     * в таблице street полный бардак. Там может быть все что угодно, они сами написали. Иерархии строгой нет. :(( сам
     * мучаюсь
     *
     * Заполняем типы и собираем поле parent_string
     */
    private function fillLevels()
    {
       /* $sql = <<<SQL
        UPDATE kladr SET level=(
          SELECT CASE
            WHEN code LIKE '%00000000000' THEN 1
            WHEN code LIKE '%00000000' THEN 2
            WHEN code LIKE '%00000' THEN 3
            ELSE 4
          END
          , CASE CASE WHEN substr(code,9,3)<>'000' THEN '4'
            WHEN substr(code,6,3)<>'000' THEN '3'
            WHEN substr(code,3,3)<>'000' THEN '2'
            ELSE '1'
               END
               WHEN '1' THEN NULL
               WHEN '2' THEN RPad(substr(CODE,1,2),11,'0')
               WHEN '3' THEN RPad(substr(CODE,1,5),11,'0')
               WHEN '4' THEN RPad(substr(CODE,1,8),11,'0')
          END  xk_Parent_Code,
        ) WHERE level IS NULL;
SQL;
        $this->db->createCommand($sql)->execute();*/
    }

    /**
     * Заполняем текстом вышестоящие уровни
     */
   /* private function fillParentString()
    {
        $sql = <<<SQL
        UPDATE kladr SET level=(
          SELECT CASE
            WHEN code LIKE '%00000000000' THEN 1
            WHEN code LIKE '%00000000' THEN 2
            WHEN code LIKE '%00000' THEN 3
            ELSE 4
          END
          FROM kladr
        ) WHERE level IS NULL;
SQL;
        $this->db->createCommand($sql)->execute();
    }*/

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
