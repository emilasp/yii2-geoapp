<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160404_080933_AddKladrIndexes extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createIndex('idx_geo_kladr_data_name', 'geo_kladr_data', 'name');
        $this->createIndex('idx_geo_kladr_data_name_full', 'geo_kladr_data', 'name_full');
        $this->createIndex('idx_geo_kladr_data_index', 'geo_kladr_data', 'index');
        $this->createIndex('idx_geo_kladr_data_count', 'geo_kladr_data', 'count');
        $this->createIndex('idx_geo_kladr_data_status', 'geo_kladr_data', 'status');
        $this->createIndex('idx_geo_kladr_data_level', 'geo_kladr_data', 'level');
        $this->createIndex('idx_geo_kladr_data_actual', 'geo_kladr_data', 'actual');

        $this->createIndex('idx_geo_kladr_data_code', 'geo_kladr_data', 'code');
        $this->createIndex('idx_geo_kladr_data_code_town', 'geo_kladr_data', 'code_town');
        $this->createIndex('idx_geo_kladr_data_code_city', 'geo_kladr_data', 'code_city');
        $this->createIndex('idx_geo_kladr_data_code_region', 'geo_kladr_data', 'code_region');
        $this->createIndex('idx_geo_kladr_data_code_subject', 'geo_kladr_data', 'code_subject');

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropIndex('idx_geo_kladr_data_name', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_name_full', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_index', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_count', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_status', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_level', 'geo_kladr_data');

        $this->dropIndex('idx_geo_kladr_data_code', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_code_town', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_code_city', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_code_region', 'geo_kladr_data');
        $this->dropIndex('idx_geo_kladr_data_code_subject', 'geo_kladr_data');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
