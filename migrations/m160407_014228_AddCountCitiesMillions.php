<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160407_014228_AddCountCitiesMillions extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=12111000 WHERE code_full=\'7700000000000\';--Moscow')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=5132000 WHERE code_full=\'7800000000000\';--Piter')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1263873 WHERE code_full=\'5200000100000\';--nn;')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1017985 WHERE code_full=\'3400000110599\';--vgd;')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1412346 WHERE code_full=\'6600000100000\';--eburg;')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1191000 WHERE code_full=\'1600000100000\';--kazan')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1035528 WHERE code_full=\'2400000100000\';--krasoyarsk')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1548000 WHERE code_full=\'5400000100000\';--novosib')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1026477 WHERE code_full=\'5900000100000\';--perm')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1109835 WHERE code_full=\'6100000100000\';--r-na-donu')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1172348 WHERE code_full=\'6300000100000\';--samara')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1166092 WHERE code_full=\'5500000100000\';--omsk')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1014610 WHERE code_full=\'3600000100000\';--voronej')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1096702 WHERE code_full=\'0200100100051\';--ufa')->execute();
        $this->db->createCommand('UPDATE geo_kladr_data SET "count"=1170000 WHERE code_full=\'7400000100000\';--chelabinsk')->execute();

        $this->afterMigrate();
    }

    public function down()
    {

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
