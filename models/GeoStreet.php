<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_street_data".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $socr
 *
 * @property string  $code
 * @property string  $code_subject
 * @property string  $code_region
 * @property string  $code_city
 * @property string  $code_town
 * @property string  $code_full
 *
 * @property string  $actual
 *
 * @property string  $index
 * @property string  $gninmb
 * @property string  $uno
 * @property string  $ocatd
 *
 * @property integer $parent_id
 * @property string  $parent_code
 * @property string  $parent_string
 */
class GeoStreet extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_street_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_kladr');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'code', 'code_subject', 'code_region', 'code_city', 'code_town', 'code_full', 'actual'],
                'required'
            ],
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 40],
            [['socr'], 'string', 'max' => 10],
            [['code'], 'string', 'max' => 16],
            [['code_subject'], 'string', 'max' => 2],
            [['code_region', 'code_city', 'code_town'], 'string', 'max' => 3],
            [['code_full'], 'string', 'max' => 17],
            [['actual'], 'string', 'max' => 2],
            [['index'], 'string', 'max' => 6],
            [['gninmb', 'uno'], 'string', 'max' => 4],
            [['ocatd'], 'string', 'max' => 11],
            [['parent_code'], 'string', 'max' => 13],
            [['parent_string'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('geoapp', 'ID'),
            'name'          => Yii::t('geoapp', 'Name'),
            'socr'          => Yii::t('geoapp', 'Socr'),
            'code'          => Yii::t('geoapp', 'Code'),
            'code_subject'  => Yii::t('geoapp', 'Code Subject'),
            'code_region'   => Yii::t('geoapp', 'Code Region'),
            'code_city'     => Yii::t('geoapp', 'Code City'),
            'code_town'     => Yii::t('geoapp', 'Code Town'),
            'code_full'     => Yii::t('geoapp', 'Code Full'),
            'actual'        => Yii::t('geoapp', 'Actual'),
            'index'         => Yii::t('geoapp', 'Index'),
            'gninmb'        => Yii::t('geoapp', 'Gninmb'),
            'uno'           => Yii::t('geoapp', 'Uno'),
            'ocatd'         => Yii::t('geoapp', 'Ocatd'),
            'parent_id'     => Yii::t('geoapp', 'Parent ID'),
            'parent_code'   => Yii::t('geoapp', 'Parent Code'),
            'parent_string' => Yii::t('geoapp', 'Parent String'),
        ];
    }
}
