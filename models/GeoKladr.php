<?php

namespace emilasp\geoapp\models;

use yii;

/**
 * This is the model class for table "kladr".
 *
 * @property string $name
 * @property string $name_type
 * @property string $name_comment
 * @property string $name_full
 *
 * @property string $socr
 *
 * @property string $code
 * @property string $code_subject
 * @property string $code_region
 * @property string $code_city
 * @property string $code_town
 * @property string $code_full
 *
 * @property string $actual
 *
 * @property string $index
 * @property string $gninmb
 * @property string $uno
 * @property string $ocatd
 * @property string $status
 *
 * @property integer $level
 * @property integer $parent_id
 * @property string $parent_code
 *
 * @property string $count
 * @property integer $is_city
 * @property string $parent_string
 */
class GeoKladr extends \emilasp\core\components\base\ActiveRecord
{
    const LEVEL_KLADR_SUBJECT = 1;
    const LEVEL_KLADR_REGION  = 2;
    const LEVEL_KLADR_CITY    = 3;
    const LEVEL_KLADR_TOWN    = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_kladr_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_kladr');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'name_type', 'name_comment', 'name_full'], 'string', 'max' => 40],
            [['socr'], 'string', 'max' => 10],
            [['code'], 'integer', 'max' => 12],
            [['code_full'], 'integer', 'max' => 13],
            [['code_subject'], 'integer', 'max' => 2],
            [['code_region', 'code_city', 'code_town',], 'integer', 'max' => 2],
            [['index'], 'string', 'max' => 6],
            [['gninmb', 'uno'], 'string', 'max' => 4],
            [['ocatd'], 'string', 'max' => 11],
            [['status', 'level', 'is_city'], 'integer', 'max' => 1],
            [['actual'], 'string', 'max' => 2],
            [['count'], 'integer'],
            [['parent_id'], 'integer'],
            [['parent_code'], 'string', 'max' => 13],
            [['parent_string'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'          => Yii::t('geo', 'Name'),
            'name_type'     => Yii::t('geo', 'Name Type'),
            'name_comment'  => Yii::t('geo', 'Name Comment'),
            'name_full'     => Yii::t('geo', 'Name Full'),
            'code'          => Yii::t('geo', 'Code'),
            'code_full'     => Yii::t('geo', 'Code Full'),
            'code_subject'  => Yii::t('geo', 'Code Subject'),
            'code_region'   => Yii::t('geo', 'Code Region'),
            'code_city'     => Yii::t('geo', 'Code City'),
            'code_town'     => Yii::t('geo', 'Code Town'),
            'socr'          => Yii::t('geo', 'Socr'),
            'index'         => Yii::t('geo', 'Index'),
            'gninmb'        => Yii::t('geo', 'Gninmb'),
            'uno'           => Yii::t('geo', 'Uno'),
            'ocatd'         => Yii::t('geo', 'Ocatd'),
            'count'         => Yii::t('geo', 'Count'),
            'parent_id'     => Yii::t('geo', 'Parent ID'),
            'parent_string' => Yii::t('geo', 'Parent String'),
            'parent_code'   => Yii::t('geo', 'Parent Code'),
            'actual'        => Yii::t('geo', 'Actual'),
            'level'         => Yii::t('geo', 'Level'),
            'status'        => Yii::t('geo', 'Status'),
            'is_city'       => Yii::t('geo', 'Город'),
        ];
    }
}
