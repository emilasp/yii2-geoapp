<?php

namespace emilasp\geoapp\controllers;

use emilasp\geoapp\models\GeoAddrobj;
use emilasp\geoapp\models\Kladr;
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityUserWidget;
use Yii;
use emilasp\core\components\base\Controller;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * KladrController
 */
class KladrController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['address', 'get-city'],
                'rules'        => [
                    [
                        'actions' => ['address', 'get-city'],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @param $search
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetCity($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $items = Kladr::find()
            ->where(['ILIKE', 'name' , $q])->limit(30)->all();
        $out = [];
        foreach ($items as $item) {
            $out[] = [
                'value' => $item->name,
                'name' => $item->socr . ' ' . $item->name,
                'index' => $item->index,
            ];
        }
        return $out;
    }



    private function getPostalCode($search)
    {
        preg_match('/^(\d{6})/i', $search, $m);
        if (isset($m[0]) && strlen($m[0]) === 6) {
            return $m[0];
        }
        return false;
    }

    /**
     * Lists all Variety models.
     * @return mixed
     */
    public function actionAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $search = Yii::$app->request->get('search');
        $index  = $this->getPostalCode($search);

        if ($index) {
            $search = str_replace($index, '', $search);
            $search = trim($search, ', ');
        }
        $data = GeoAddrobj::getObjectsByName($search, $index);

        $data = ArrayHelper::map($data, 'aoid', 'formalname');
        $out  = [];
        foreach ($data as $index => $formalname) {
            $out[] = ['aoid' => $index, 'formalname' => $formalname,];
        }
        return $out;
    }


    /** Пользовательскийц выбор города
     * @return string
     * @throws \Exception
     */
    public function actionSelectCity() {
        return SelectCityUserWidget::widget([
            'value' => Yii::$app->request->get('value')
        ]);
    }
}
