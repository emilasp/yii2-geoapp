<?php

namespace emilasp\geoapp\controllers;

use emilasp\geoapp\models\GeoAddrobj;
use Yii;
use emilasp\core\components\base\Controller;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * FiasController
 */
class FiasController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['address'],
                'rules'        => [
                    [
                        'actions' => ['address'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    private function getPostalCode($search)
    {
        preg_match('/^(\d{6})/i', $search, $m);
        if (isset($m[0]) && strlen($m[0]) === 6) {
            return $m[0];
        }
        return false;
    }

    /**
     * Lists all Variety models.
     * @return mixed
     */
    public function actionAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $search = Yii::$app->request->get('search');
        $index  = $this->getPostalCode($search);

        if ($index) {
            $search = str_replace($index, '', $search);
            $search = trim($search, ', ');
        }
        $data = GeoAddrobj::getObjectsByName($search, $index);

        $data = ArrayHelper::map($data, 'aoid', 'formalname');
        $out  = [];
        foreach ($data as $index => $formalname) {
            $out[] = ['aoid' => $index, 'formalname' => $formalname,];
        }
        return $out;
    }


    /**
     * @param null $q
     * @param null $id
     *
     * @return array
     */
    public function actionSelectCity($q = null, $id = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = GeoAddrobj::getCityByName($q, null);
            $data = ArrayHelper::map($data, 'aoid', 'formalname');
            $out = ['results' => []];
            foreach ($data as $index => $formalname) {
                $out['results'][] = ['id' => $index, 'text' => $formalname,];
            }
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => GeoAddrobj::find($id)->formalname];
        }

        return $out;
    }
}
