<?php

namespace emilasp\geoapp\controllers;

use emilasp\geoapp\models\Countries;
use emilasp\geoapp\models\search\CountriesSearch;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * CountriesController implements the CRUD actions for Countries model.
 */
class CountriesController extends \emilasp\core\components\CController
{
	/**
	 * Lists all Countries models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new CountriesSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Countries model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($country_id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($country_id),
		]);
	}

	/**
	 * Creates a new Countries model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Countries;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Countries model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($country_id)
	{
		$model = $this->findModel($country_id);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Countries model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($country_id)
	{
		$this->findModel($country_id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Countries model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Countries the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($country_id)
	{
		if (($model = Countries::findOne($country_id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
