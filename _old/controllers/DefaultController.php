<?php

namespace emilasp\geoapp\controllers;

use yii\filters\VerbFilter;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'upload' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'upload' => [
                'class' => 'backend\modules\site\back\actions\ckeeditor\CkeUploadAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
