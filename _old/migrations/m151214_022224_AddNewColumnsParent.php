<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151214_022224_AddNewColumnsParent extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        /*$this->db->createCommand("ALTER TABLE geo_addrobj ADD COLUMN parentname  CHARACTER VARYING(120) NULL;")->execute();

        $this->db->createCommand("ALTER TABLE geo_addrobj ADD COLUMN parentparentaoid  CHARACTER VARYING(36) NULL;")->execute();

        $this->db->createCommand("ALTER TABLE geo_addrobj ADD COLUMN parentparentname  CHARACTER VARYING(120) NULL;")->execute();

        $this->db->createCommand("UPDATE geo_addrobj
SET
  parentname       = b.gcityname,
  parentparentguid = b.gregionaoid,
  parentparentname = b.gregionname
FROM test_view AS b
WHERE b.aoid = geo_addrobj.aoid;")->execute();*/


        $this->afterMigrate();
    }

    public function down()
    {
        //$this->dropTable('files_file');

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
