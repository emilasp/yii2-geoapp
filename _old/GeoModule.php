<?php

namespace emilasp\geoapp;

use emilasp\core\CoreModule;

class GeoModule extends CoreModule
{
    /**
     * Разрешённые страны
     * @var array
     */
    public $countryAllow;

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
