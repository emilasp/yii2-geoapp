<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var emilasp\geoapp\models\Country $model
 */

$this->title = 'Create Country';
$this->params['breadcrumbs'][] = ['label' => 'Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
