<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;
use \kartik\helpers\Html;
use kartik\icons\Icon;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var emilasp\geoapp\models\search\CitySearch $searchModel
 */

$this->title = 'Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">



    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>
    <p>
        <?php /* echo Html::a('Create City', ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>


    <?php     echo 
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'showPageSummary' => true,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],


            [
                'attribute' => 'id',
                'class' => '\kartik\grid\DataColumn',
                'width'=>'100px',
                'hAlign'=>GridView::ALIGN_CENTER,
                'vAlign'=>GridView::ALIGN_MIDDLE,
            ],
            
/*            'country_id',*/ 
/*            'region_id',*/ 
            'name',

            [
                'class' => '\kartik\grid\ActionColumn',

            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('site', 'Add'), ['create'], ['class' => 'btn btn-success']).' '.Html::button( '<i class="glyphicon glyphicon-search"></i> '.Yii::t('app','Search'), [ 'type'=>'button', 'onClick'=>'$(\'.indexSearch\').toggle(\'slow\');', 'class' => 'btn btn-primary']),
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]);

    Pjax::end(); ?>

</div>
