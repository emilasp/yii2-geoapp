<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;
use \kartik\helpers\Html;
use kartik\icons\Icon;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var emilasp\geoapp\models\search\RegionSearch $searchModel
 */

$this->title = 'Regions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-index">



    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>
    <p>
        <?php /* echo Html::a('Create Region', ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>


    <?php     echo 
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'showPageSummary' => true,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],


            [
                'attribute' => 'id',
                'class' => '\kartik\grid\DataColumn',
                'width'=>'100px',
                'hAlign'=>GridView::ALIGN_CENTER,
                'vAlign'=>GridView::ALIGN_MIDDLE,
            ],
            
/*            'country_id',*/ 

            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'city_id',
                'value' => function ($model, $key, $index, $column){
                        if(!is_null($model->cities))
                            return $model->cities->name;
                        return $model->city_id;
                    },
                'hidden'=>!\Yii::$app->getModule('site')->enableCity,
                'hAlign'=>GridView::ALIGN_LEFT,
                'vAlign'=>GridView::ALIGN_MIDDLE,
                'width'=>'150px',
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                   'language' => \Yii::$app->language,
                    'addon' => ['prepend' => [ 'content' => Icon::show('home',[], Icon::BSG) ]],
                    'options' => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['/geoapp/cities/select']),
                            'dataType' => 'json',
                            'data' => new \yii\web\JsExpression('function(term,page) {console.log(term); return {search:term}; }'),
                            'results' => new \yii\web\JsExpression('function(data,page) { console.log(data.results); return {results:data.results}; }'),
                        ],
                        'initSelection' => new \yii\web\JsExpression('function (element, callback) { var id=$(element).val(); if (id !== "") { $.ajax("'.\yii\helpers\Url::to(['/geoapp/cities/select']).'?id=" + id, { dataType: "json" }).done(function(data) { callback(data.results);}); } }')
                    ],
                ],
            ],
            
            'name',

            [
                'class' => '\kartik\grid\ActionColumn',

            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('site', 'Add'), ['create'], ['class' => 'btn btn-success']).' '.Html::button( '<i class="glyphicon glyphicon-search"></i> '.Yii::t('app','Search'), [ 'type'=>'button', 'onClick'=>'$(\'.indexSearch\').toggle(\'slow\');', 'class' => 'btn btn-primary']),
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]);

    Pjax::end(); ?>

</div>
