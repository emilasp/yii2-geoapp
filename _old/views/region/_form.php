<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\widgets\Select2;
use emilasp\core\components\OptionsData;
use kartik\icons\Icon;

/**
 * @var yii\web\View $this
 * @var emilasp\geoapp\models\Region $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="region-form">


    <?php 
    $form = \kartik\widgets\ActiveForm::begin([
        'type' => \kartik\widgets\ActiveForm::TYPE_VERTICAL,
        'options'=>['enctype'=>'multipart/form-data'],
        'formConfig' => ['autoPlaceholder' => true, 'labelSpan' => 2, 'deviceSize' => \kartik\widgets\ActiveForm::SIZE_SMALL]
    ]);
    ?>


    <div class="row">
        <div class="col-md-12">
<?php 
     echo emilasp\form\inputs\Number::widget([
                         'form'=>$form, 'model'=>$model, 'attribute'=>'country_id',
                         'options'=>[
                            'pluginOptions'=>[
                                 'postfix' => '%',
                                 //'prefix' => '$',
                                 'min' => 0,
                                 'max' => 100000,
                                 'step' => 1,
                                 'decimals' => 0,
                                 'boostat' => 5,
                                 'maxboostedstep' => 10,
                                 'verticalbuttons' => true
                             ],
                         ]
                     ]); 

?>
        </div>
    </div>

    

    <div class="row">
        <div class="col-md-12">
<?php 
     echo ((\Yii::$app->getModule('site')->enableCity)?emilasp\form\inputs\Select::widget([
                        'form'=>$form, 'model'=>$model, 'attribute'=>'city_id',
                        'options'=>[
                            'language' => \Yii::$app->language,
                            'addon' => ['prepend' => [ 'content' => Icon::show('home',[], Icon::BSG) ]],
                            'options' => ['placeholder' => '-выбрать-'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['/geoapp/cities/select']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(term,page) {console.log(term); return {search:term}; }'),
                                    'results' => new \yii\web\JsExpression('function(data,page) { console.log(data.results); return {results:data.results}; }'),
                                ],
                                'initSelection' => new \yii\web\JsExpression('function (element, callback) { var id=$(element).val(); if (id !== "") { $.ajax("'.\yii\helpers\Url::to(['/geoapp/cities/select']).'?id=" + id, { dataType: "json" }).done(function(data) { callback(data.results);}); } }')
                            ],
                        ]
                    ]):''); 

?>
        </div>
    </div>

    

    <div class="row">
        <div class="col-md-12">
<?php 
     echo emilasp\form\inputs\String::widget([
                        'form'=>$form, 'model'=>$model, 'attribute'=>'name',
                        'inputOptions'=>[ 'class'=>'form-control', ],
                        'options'=>[
                            'addon' => [
                                'prepend'=>['content' => Icon::show('pushpin',[], Icon::BSG), 'options'=>['class'=>'alert-success']],
                                //'append' => ['content' => Icon::show('chevron-right',[], Icon::BSG), 'options'=>['style' => 'font-family: Monaco, Consolas, monospace;']],
                            ]
                        ]
                    ]); 

?>
        </div>
    </div>

    
    
    <?php 
    ?>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>

            <?php 
            if( $model->isNewRecord ){
                echo Html::button('<span class="glyphicon glyphicon-forward"></span> '.Yii::t('site', 'Create and next'), ['onClick'=>'$(this).parent().parent().parent().attr("action",$(this).parent().parent().parent().attr("action")+"/?next");$(this).parent().parent().parent().submit();','class' => 'btn btn-success' ]);
            }
            ?>



        </div>
    </div>

<?php     \kartik\widgets\ActiveForm::end();
?>

</div>
