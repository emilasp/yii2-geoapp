<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var emilasp\geoapp\models\Region $model
 */

$this->title = 'Create Region';
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
