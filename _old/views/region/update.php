<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var emilasp\geoapp\models\Region $model
 */

$this->title = 'Update Region: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="region-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
