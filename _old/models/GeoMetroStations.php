<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_metro_stations".
 *
 * @property integer $id
 * @property integer $line_id
 * @property integer $order
 * @property double $map_point_lat
 * @property double $map_point_lng
 * @property string $name
 * @property string $name_lat
 * @property integer $b_active
 *
 * @property GeoMetroLines $line
 * @property GeoMetroStationsNear[] $geoMetroStationsNears
 */
class GeoMetroStations extends \emilasp\core\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_metro_stations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['line_id', 'order'], 'required'],
            [['line_id', 'order', 'b_active'], 'integer'],
            [['map_point_lat', 'map_point_lng'], 'number'],
            [['name'], 'string', 'max' => 63],
            [['name_lat'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'line_id' => 'Line ID',
            'order' => 'Order',
            'map_point_lat' => 'Map Point Lat',
            'map_point_lng' => 'Map Point Lng',
            'name' => 'Name',
            'name_lat' => 'Name Lat',
            'b_active' => 'B Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLine()
    {
        return $this->hasOne(GeoMetroLines::className(), ['id' => 'line_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoMetroStationsNears()
    {
        return $this->hasMany(GeoMetroStationsNear::className(), ['metroNearId' => 'id']);
    }
}
