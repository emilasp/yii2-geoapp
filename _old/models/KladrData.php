<?php

namespace emilasp\geoapp\models;

use yii;

/**
 * This is the model class for table "kladr".
 *
 * @property string $name
 * @property string $name_type
 * @property string $name_comment
 * @property string $name_full
 *
 * @property string $socr
 *
 * @property string $code
 * @property string $code_subject
 * @property string $code_region
 * @property string $code_city
 * @property string $code_town
 * @property string $code_full
 *
 * @property string $actual
 *
 * @property string $index
 * @property string $gninmb
 * @property string $uno
 * @property string $ocatd
 * @property string $status
 *
 * @property integer $level
 * @property string $parent_code
 *
 * @property string $count
 * @property string $parent_string
 */
class KladrData extends \emilasp\core\components\base\ActiveRecord
{
    const LEVEL_KLADR_REGION   = 1;
    const LEVEL_KLADR_DISTRICT = 2;
    const LEVEL_KLADR_CITY     = 3;
    const LEVEL_KLADR_TOWNSHIP = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_kladr_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_kladr');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 40],
            [['socr'], 'string', 'max' => 10],
            [['code'], 'integer', 'max' => 13],
            [['index'], 'string', 'max' => 6],
            [['gninmb', 'uno'], 'string', 'max' => 4],
            [['ocatd'], 'string', 'max' => 11],
            [['status'], 'string', 'max' => 1],
            [['level'], 'integer', 'max' => 1],
            [['count'], 'integer'],
            [['parent_id'], 'integer'],
            [['parent_string'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'          => Yii::t('geo', 'Name'),
            'code'          => Yii::t('geo', 'Code'),
            'socr'          => Yii::t('geo', 'Socr'),
            'index'         => Yii::t('geo', 'Index'),
            'gninmb'        => Yii::t('geo', 'Gninmb'),
            'uno'           => Yii::t('geo', 'Uno'),
            'ocatd'         => Yii::t('geo', 'Ocatd'),
            'count'         => Yii::t('geo', 'Count'),
            'parent_id'     => Yii::t('geo', 'Parent ID'),
            'parent_string' => Yii::t('geo', 'Parent String'),
            'level'         => Yii::t('geo', 'Level'),
            'status'        => Yii::t('geo', 'Status'),
        ];
    }
}
