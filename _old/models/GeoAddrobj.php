<?php

namespace emilasp\geoapp\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "geo_addrobj".
 *
 * @property string $aoid
 * @property string $formalname
 * @property string $regioncode
 * @property string $autocode
 * @property string $areacode
 * @property string $citycode
 * @property string $ctarcode
 * @property string $placecode
 * @property string $streetcode
 * @property string $extrcode
 * @property string $sextcode
 * @property string $offname
 * @property string $postalcode
 * @property string $ifnsfl
 * @property string $terrifnsfl
 * @property string $ifnsul
 * @property string $terrifnsul
 * @property string $okato
 * @property string $oktmo
 * @property string $updatedate
 * @property string $shortname
 * @property integer $aolevel
 * @property string $parentguid
 * @property string $aoguid
 * @property string $previd
 * @property string $nextid
 * @property string $code
 * @property string $plaincode
 * @property integer $actstatus
 * @property integer $centstatus
 * @property integer $operstatus
 * @property integer $currstatus
 * @property string $startdate
 * @property string $enddate
 * @property string $normdoc
 * @property string $parentname
 * @property string $parentparentaoid
 * @property string $parentparentname
 */
class GeoAddrobj extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_addrobj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'aoid',
                    'formalname',
                    'regioncode',
                    'autocode',
                    'areacode',
                    'citycode',
                    'ctarcode',
                    'placecode',
                    'streetcode',
                    'extrcode',
                    'sextcode',
                    'offname',
                    'postalcode',
                    'ifnsfl',
                    'terrifnsfl',
                    'ifnsul',
                    'terrifnsul',
                    'okato',
                    'oktmo',
                    'updatedate',
                    'shortname',
                    'aolevel',
                ],
                'required',
            ],
            [['updatedate', 'startdate', 'enddate'], 'safe'],
            [['aolevel', 'actstatus', 'centstatus', 'operstatus', 'currstatus'], 'integer'],
            [['aoid', 'parentguid', 'aoguid', 'previd', 'nextid', 'normdoc', 'parentparentaoid'], 'string', 'max' => 36],
            [['formalname', 'offname', 'parentname', 'parentparentname'], 'string', 'max' => 120],
            [['regioncode'], 'string', 'max' => 2],
            [['autocode'], 'string', 'max' => 1],
            [['areacode', 'citycode', 'ctarcode', 'placecode', 'sextcode'], 'string', 'max' => 3],
            [['streetcode', 'extrcode', 'ifnsfl', 'terrifnsfl', 'ifnsul', 'terrifnsul'], 'string', 'max' => 4],
            [['postalcode'], 'string', 'max' => 6],
            [['okato'], 'string', 'max' => 11],
            [['oktmo'], 'string', 'max' => 8],
            [['shortname'], 'string', 'max' => 10],
            [['code'], 'string', 'max' => 17],
            [['plaincode'], 'string', 'max' => 15],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aoid'       => Yii::t('geo', 'Aoid'),
            'formalname' => Yii::t('geo', 'Formalname'),
            'regioncode' => Yii::t('geo', 'Regioncode'),
            'autocode'   => Yii::t('geo', 'Autocode'),
            'areacode'   => Yii::t('geo', 'Areacode'),
            'citycode'   => Yii::t('geo', 'Citycode'),
            'ctarcode'   => Yii::t('geo', 'Ctarcode'),
            'placecode'  => Yii::t('geo', 'Placecode'),
            'streetcode' => Yii::t('geo', 'Streetcode'),
            'extrcode'   => Yii::t('geo', 'Extrcode'),
            'sextcode'   => Yii::t('geo', 'Sextcode'),
            'offname'    => Yii::t('geo', 'Offname'),
            'postalcode' => Yii::t('geo', 'Postalcode'),
            'ifnsfl'     => Yii::t('geo', 'Ifnsfl'),
            'terrifnsfl' => Yii::t('geo', 'Terrifnsfl'),
            'ifnsul'     => Yii::t('geo', 'Ifnsul'),
            'terrifnsul' => Yii::t('geo', 'Terrifnsul'),
            'okato'      => Yii::t('geo', 'Okato'),
            'oktmo'      => Yii::t('geo', 'Oktmo'),
            'updatedate' => Yii::t('geo', 'Updatedate'),
            'shortname'  => Yii::t('geo', 'Shortname'),
            'aolevel'    => Yii::t('geo', 'Aolevel'),
            'parentguid' => Yii::t('geo', 'Parentguid'),
            'aoguid'     => Yii::t('geo', 'Aoguid'),
            'previd'     => Yii::t('geo', 'Previd'),
            'nextid'     => Yii::t('geo', 'Nextid'),
            'code'       => Yii::t('geo', 'Code'),
            'plaincode'  => Yii::t('geo', 'Plaincode'),
            'actstatus'  => Yii::t('geo', 'Actstatus'),
            'centstatus' => Yii::t('geo', 'Centstatus'),
            'operstatus' => Yii::t('geo', 'Operstatus'),
            'currstatus' => Yii::t('geo', 'Currstatus'),
            'startdate'  => Yii::t('geo', 'Startdate'),
            'enddate'    => Yii::t('geo', 'Enddate'),
            'normdoc'    => Yii::t('geo', 'Normdoc'),
            'parentname'    => Yii::t('geo', 'Parentname'),
            'parentparentaoid'    => Yii::t('geo', 'Parentparentaoid'),
            'parentparentname'    => Yii::t('geo', 'Parentparentname'),
        ];
    }


    /** Получаем объекты по индексу
     *
     * @param $postal
     *
     * @return static[]
     */
    public static function getObjectsByPostalCode($postal)
    {
        $objects = GeoAddrobj::find()->filterWhere(['LIKE', 'postalcode', $postal])
                             ->limit(25)->all();
        return $objects;
    }

    /** Получаем объекты по AOID
     *
     * @param $aoid
     *
     * @return static[]
     */
    public static function getObjectsByAoid($aoid)
    {
        $objects = GeoAddrobj::findAll(['aoid' => $aoid]);
        return $objects;
    }

    /** Получаем объекты по AOID
     *
     * @param $name
     * @param $index
     *
     * @return static[]
     */
    public static function getObjectsByName($name, $index)
    {

        $condPostalCode = '';
        if ($index) {
            $condPostalCode = " AND postalcode = '{$index}' ";
        }

        $sql = <<<SQL
SELECT
  aoid,
  postalcode,
  CONCAT(shortname, '. ', formalname) AS name,
  aolevel,
  aoguid,
  parentguid,
  parentname,
  parentparentname
FROM geo_addrobj
WHERE formalname ~* :search
{$condPostalCode}
AND actstatus = 1
AND aolevel IN (6,7)
ORDER BY parentparentname, parentname, postalcode
LIMIT 200;
SQL;

        $objects = Yii::$app->db->createCommand($sql)->bindParam(':search', $name)->queryAll();

        $return  = [];
        foreach ($objects as $obj) {
            $objArr = [
                'aoid'       => $obj['aoid'],
                'formalname' => self::getAddressString($obj),
            ];
            $return[] = $objArr;
        }
        return $return;
    }



    /** Получаем объекты по AOID
     *
     * @param $name
     * @param $index
     * @param $region
     *
     * @return static[]
     */
    public static function getCityByName($name, $index, $region = [1])
    {

        $condPostalCode = '';
        if ($index) {
            $condPostalCode = " AND postalcode = '{$index}' ";
        }

        $region = implode(',', $region);

        $sql = <<<SQL
SELECT
  aoid,
  postalcode,
  formalname,
  CONCAT(shortname, '. ', formalname) AS name,
  aolevel,
  aoguid,
  parentguid,
  parentname,
  parentparentname
FROM geo_addrobj
WHERE formalname ~* :search
{$condPostalCode}
AND actstatus = 1
AND aolevel IN ({$region})
ORDER BY parentparentname, parentname, postalcode
LIMIT 200;
SQL;

        $objects = Yii::$app->db->createCommand($sql)->bindParam(':search', $name)->queryAll();

        $return  = [];
        foreach ($objects as $obj) {
            $parentName = $obj['parentname']?'(' . $obj['parentname'] . ')':'';

            $objArr = [
                'aoid'       => $obj['aoid'],
                'formalname' => $obj['formalname'] . $parentName,
            ];
            $return[] = $objArr;
        }
        return $return;
    }

    private static function getAddressString($obj)
    {
        $address = '';
        if ($obj['postalcode']) {
            $address .= $obj['postalcode'] . ', ';
        }
        if ($obj['parentparentname']) {
            $address .= $obj['parentparentname'] . ', ';
        }
        if ($obj['parentname']) {
            $address .= $obj['parentname'] . ', ';
        }
        $address .= $obj['name'];
        return $address;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['aoid' => 'parentguid']);
    }
}
