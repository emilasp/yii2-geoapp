<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_city_list".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_gen
 * @property string $name_dat
 * @property string $name_acc
 * @property string $name_abl
 * @property string $name_pre
 * @property string $map_center
 * @property integer $country_id
 * @property string $name_en
 * @property integer $yandexRegionId
 * @property string $oblName
 * @property integer $bNoDistricts
 * @property string $oblNamePre
 * @property string $oblNameEn
 * @property integer $bRegionCenter
 * @property string $oblNameGen
 * @property integer $regionId
 *
 * @property GeoCountryRegions $region
 * @property GeoCountry $country
 * @property GeoCityRegions[] $geoCityRegions
 */
class GeoCity extends \emilasp\core\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_city_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'regionId'], 'required'],
            [['country_id', 'yandexRegionId', 'bNoDistricts', 'bRegionCenter', 'regionId'], 'integer'],
            [['name', 'oblNamePre'], 'string', 'max' => 255],
            [['name_gen', 'name_dat', 'name_acc', 'name_abl', 'name_pre', 'map_center', 'name_en', 'oblName', 'oblNameEn', 'oblNameGen'], 'string', 'max' => 63]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_gen' => 'Name Gen',
            'name_dat' => 'Name Dat',
            'name_acc' => 'Name Acc',
            'name_abl' => 'Name Abl',
            'name_pre' => 'Name Pre',
            'map_center' => 'Map Center',
            'country_id' => 'Country ID',
            'name_en' => 'Name En',
            'yandexRegionId' => 'Yandex Region ID',
            'oblName' => 'Obl Name',
            'bNoDistricts' => 'B No Districts',
            'oblNamePre' => 'Obl Name Pre',
            'oblNameEn' => 'Obl Name En',
            'bRegionCenter' => 'B Region Center',
            'oblNameGen' => 'Obl Name Gen',
            'regionId' => 'Region ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoCountryRegions::className(), ['id' => 'regionId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCityRegions()
    {
        return $this->hasMany(GeoCityRegions::className(), ['city_id' => 'id']);
    }
}
