<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_country".
 *
 * @property integer $id
 * @property string $name
 *
 * @property GeoCityList[] $geoCityLists
 * @property GeoCountryRegions[] $geoCountryRegions
 */
class GeoCountry extends \emilasp\core\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCityLists()
    {
        return $this->hasMany(GeoCityList::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCountryRegions()
    {
        return $this->hasMany(GeoCountryRegions::className(), ['countryId' => 'id']);
    }
}
