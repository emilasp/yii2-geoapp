<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_metro_stations_near".
 *
 * @property integer $id
 * @property integer $metroCenterId
 * @property integer $metroNearId
 * @property double $distance
 *
 * @property GeoMetroStations $metroCenter
 * @property GeoMetroStations $metroNear
 */
class GeoMetroStationsNear extends \emilasp\core\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_metro_stations_near';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metroCenterId', 'metroNearId', 'distance'], 'required'],
            [['metroCenterId', 'metroNearId'], 'integer'],
            [['distance'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'metroCenterId' => 'Metro Center ID',
            'metroNearId' => 'Metro Near ID',
            'distance' => 'Distance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetroCenter()
    {
        return $this->hasOne(GeoMetroStations::className(), ['id' => 'metroCenterId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetroNear()
    {
        return $this->hasOne(GeoMetroStations::className(), ['id' => 'metroNearId']);
    }
}
