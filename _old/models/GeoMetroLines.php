<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_metro_lines".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 * @property integer $city_id
 *
 * @property GeoMetroStations[] $geoMetroStations
 */
class GeoMetroLines extends \emilasp\core\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_metro_lines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color', 'city_id'], 'required'],
            [['city_id'], 'integer'],
            [['name'], 'string', 'max' => 31],
            [['color'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'color' => 'Color',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoMetroStations()
    {
        return $this->hasMany(GeoMetroStations::className(), ['line_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(GeoCityList::className(), ['id' => 'city_id']);
    }

}
