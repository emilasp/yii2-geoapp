<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_country_regions".
 *
 * @property integer $id
 * @property integer $countryId
 * @property string $name
 * @property string $nameGen
 * @property string $namePre
 * @property string $nameEn
 * @property string $code
 *
 * @property GeoCityList[] $geoCityLists
 * @property GeoCountry $country
 */
class GeoCountryRegions extends \emilasp\core\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_country_regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryId', 'name', 'nameEn'], 'required'],
            [['countryId'], 'integer'],
            [['name', 'nameGen', 'namePre', 'nameEn'], 'string', 'max' => 63],
            [['code'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'countryId' => 'Country ID',
            'name' => 'Name',
            'nameGen' => 'Name Gen',
            'namePre' => 'Name Pre',
            'nameEn' => 'Name En',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoCityLists()
    {
        return $this->hasMany(GeoCityList::className(), ['regionId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::className(), ['id' => 'countryId']);
    }
}
