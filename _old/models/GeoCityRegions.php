<?php

namespace emilasp\geoapp\models;

use Yii;

/**
 * This is the model class for table "geo_city_regions".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 * @property string $name_short
 * @property string $name_in_short
 * @property integer $type
 * @property string $name_en
 * @property string $name_gen
 *
 * @property GeoCityList $city
 */
class GeoCityRegions extends \emilasp\core\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_city_regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id', 'type'], 'integer'],
            [['name', 'name_in_short', 'name_en'], 'string', 'max' => 255],
            [['name_short'], 'string', 'max' => 32],
            [['name_gen'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city_id' => 'City ID',
            'name_short' => 'Name Short',
            'name_in_short' => 'Name In Short',
            'type' => 'Type',
            'name_en' => 'Name En',
            'name_gen' => 'Name Gen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(GeoCityList::className(), ['id' => 'city_id']);
    }
}
