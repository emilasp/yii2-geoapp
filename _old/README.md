Модуль GeoIp для Yii2
=============================

Модуль GeoIp.

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-geoapp": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-geoapp.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'geoapp' => []
    ],
```
