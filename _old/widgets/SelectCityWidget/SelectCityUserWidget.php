<?php
namespace emilasp\geoapp\widgets\SelectCityWidget;

use emilasp\geoapp\models\Kladr;
use kartik\typeahead\TypeaheadAsset;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;
use yii\widgets\InputWidget;

/**
 * Автокомплит адрес
 *
 * Class SelectCityUserWidget
 * @package emilasp\geoapp\widgets\SelectCityWidget
 */
class SelectCityUserWidget extends SelectCityWidget
{
    const COOKIE_KEY = 'city';

    public $model;
    public $attribute;
    public $label;

    public function init()
    {
        parent::init();
        $this->registerAssets();

        $this->setUserCity();
        $this->setUserCityFromCookie();

        $this->setLabel();
    }

    public function run()
    {
        echo $this->render('select-city', [
            'id'    => $this->id,
            'label' => $this->label,
            'value' => $this->value,
        ]);
        if ($this->model) {
            echo Html::activeHiddenInput($this->model, $this->attribute);
        } else {
            echo Html::textInput($this->name, $this->value);
        }
    }

    /**
     * Устаналиваем название выбранного города для вьюхи
     */
    private function setLabel()
    {
        if (!$this->value) {
            $this->label = Yii::t('im', '-select-');
        } else {
            $model = Kladr::find()->where(['code' => $this->value])->one();
            $this->label = $model->name;
        }
    }

    /**
     * Устанавливаем город пользователя из куки
     */
    private function setUserCityFromCookie()
    {
        $this->value = Yii::$app->request->cookies->getValue(self::COOKIE_KEY, '');
    }


    /**
     *  Устанавливаем город для пользователя в куку
     */
    private function setUserCity()
    {
        if (Yii::$app->request->isPjax) {
            Yii::$app->response->cookies->add(new Cookie([
                'name'  => self::COOKIE_KEY,
                'value' => $this->value,
            ]));
        }
    }

    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        SelectCityWidgetAsset::register($this->view);
    }
}
