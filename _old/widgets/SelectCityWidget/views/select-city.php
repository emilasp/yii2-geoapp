<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
?>

<?= Pjax::begin(['id' => 'pjax-select-city']) ?>
<span class="select-city">
    <span class="select-city-label"><?= $label ?></span>
    <div class="select-city-modal" style="display: none">
        <div class="cities-millions">
            <?php foreach ($citiesMillions as $city) : ?>
                <div class="city-name city-column">
                    <label>
                        <?= Html::radio('city-millions[]', $value === $city->id, [
                            'value' => $city->code
                        ]) ?>
                        <?= $city->name ?>
                    </label>
                </div>
            <?php endforeach ?>
        </div>

        <div class="text-right">
            <button type="button" class="btn btn-success btn-change-city"><?= Yii::t('im', 'Save') ?></button>
        </div>

    </div>
</span>
<?= Pjax::end() ?>

<?php

$urlToSaveCity = Url::toRoute('/geo/kladr/select-city');

$js = <<<JS
    $('.cities-millions input:checked').closest('label').addClass('city-selected');

    $('body').on('change', '.cities-millions input:checked', function () {
        var input = $(this);
        var inputs = input.closest('.cities-millions').find('input');

        inputs.removeClass('city-selected');
        input.addClass('city-selected');


    });

    $('body').on('click', '.btn-change-city', function () {
        var input = $('.cities-millions input:checked');
        var value = input.val();
        $.pjax({
            container: "#files-blocks",
            timeout : 0,
            url: "{$urlToSaveCity}",
            data: {value:value},
            push:false
        });

    });
JS;
$this->registerJs($js);