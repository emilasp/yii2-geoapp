<?php
namespace emilasp\geoapp\widgets\SelectCityWidget;

use emilasp\geoapp\models\Kladr;
use kartik\typeahead\TypeaheadAsset;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;

/**
 * Автокомплит адрес
 *
 * Class SelectCityWidget
 * @package emilasp\geoapp\widgets\SelectCityWidget
 */
class SelectCityWidget extends InputWidget
{
    public $model;
    public $attribute;
    public $label;

    public $countMillions = 10;


    public function init()
    {
        parent::init();
        $this->registerAssets();

        $this->setUserCityFromCookie();

        if (!$this->value) {
            $this->label = Yii::t('im', '-select-');
        } else {
            if ($this->model) {
                $model = Kladr::find()->where(['code' => $this->model->{$this->attribute}])->one();
            } else {
                $model = Kladr::find()->where(['code' => $this->value])->one();
            }
            $this->label = $model->name;
        }
    }

    public function run()
    {
        echo $this->render('select-city', [
            'id'             => $this->id,
            'label'          => $this->label,
            'value'          => $this->value,
            'citiesMillions' => $this->getCitiesMillions(),
        ]);
        if ($this->model) {
            echo Html::activeHiddenInput($this->model, $this->attribute);
        } else {
            echo Html::textInput($this->name, $this->value);
        }
    }


    /** Получаем города миллионики
     * @return array|yii\db\ActiveRecord[]
     */
    private function getCitiesMillions()
    {
        return Kladr::find()->where(['type' => Kladr::TYPE_CITY])->orderBy('count DESC')->limit($this->countMillions)->all();
    }


    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        SelectCityWidgetAsset::register($this->view);

        $title = Yii::t('im', 'Please select you city');

        $js = <<<JS
        $('.select-city-label').jBox('Modal', {
            title: '{$title}',
            content: $('.select-city-modal')
        });
JS;
        $this->view->registerJs($js);
    }
}
