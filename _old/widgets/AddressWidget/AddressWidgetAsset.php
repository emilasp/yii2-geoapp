<?php
namespace emilasp\geoapp\widgets\AddressWidget;

use yii\web\AssetBundle;

/**
 * Class AddressWidgetAsset
 * @package emilasp\im\common\widgets\AddressWidget
 */
class AddressWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = ['address-input.js'];
    public $css = ['address-input.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'emilasp\core\assets\AutoCompleteAsset',
    ];
}
