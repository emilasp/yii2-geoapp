<?php
namespace emilasp\geoapp\widgets\AddressWidget;

use kartik\typeahead\TypeaheadAsset;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;

/**
 * Автокомплит адрес
 *
 * Class AddressWidget
 * @package emilasp\geoapp\widgets\AddressWidget
 */
class AddressWidget extends InputWidget
{
    public $form;
    public $model;
    public $attribute;

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->render('address', [
            'id'        => $this->id,
            'model'     => $this->model,
            'attribute' => $this->attribute,
        ]);
        echo $this->form->field($this->model, $this->attribute)->hiddenInput(['id' => 'address-input-' . $this->id])->label(false);
    }

    /**
     *  Генерируем строки из значения свойства модели
     */
    private function generateRows()
    {
        $data = $this->model->{$this->attribute};

        $data = (array)json_decode($data, true);
        foreach ($data as $index => $row) {
            $data[$index] = ArrayHelper::merge($this->schemeRow, $row);
        }

        $js = <<<JS
        $("#{$this->id} input.fias").fias({id:'{$this->id}'});
JS;

        $this->view->registerJs($js);
        $this->rows = $data;
    }

    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {

        $jsHead = <<<JS
        function updateAddress(id)
        {

             console.log('dfs');

          var trs = $("#{$this->id} tr.row-address");

          obj = {};

          trs.each(function(index, el){
              obj[index] = {};
              var aoid = $(el).find('.fias').data('aoid');
              var address = $(el).find('.fias-address').typeahead('val');
              var zip = $(el).find('.fias-zip').val();

              console.log(address);
              obj[index]['aoid'] = aoid;
              obj[index]['zip'] = zip;
              obj[index]['address'] = address;
          });

          $("#multi-input-{$this->id}").val(JSON.stringify(obj));
        }
JS;
        $js = <<<JS
        $("#{$this->id} input.fias-address").bind('change', function() {

        });

        $('#{$this->id}').on('click', '.btn-add', function(ev, suggestion) {
              var block = $(this).parent().parent().parent().parent().parent();
              var inputs = block.find('.fias');
              var maxCount = 0;

              console.log(block);

              inputs.each(function (index, el) {
                var input = $(el);
                if (maxCount<input.data('count')) {
                    maxCount = input.data('count');
                }
              });
              maxCount++;

              var newBlock =  $('<tr class="row-address"><td>'
                +'<input type="text" data-aoid="" data-index="" data-address="" class="fias"'
                +'data-count="' + maxCount +'" id="{$this->id}' + maxCount +'" /></td><td>'
                +'<button type="button" class="btn btn-danger btn-delete">-</button>'
                +'</td></tr>');
                newBlock.appendTo(block.find('table'));
                newBlock.find('.fias').fias();
        });

        $('#{$this->id}').on('click', '.btn-delete', function(ev, suggestion) {
              var tr = $(this).parent().parent();
              tr.remove();
              updateAddress('{$this->id}');

        });


JS;

        $this->view->registerJs($jsHead, yii\web\View::POS_HEAD);
        $this->view->registerJs($js);
        TypeaheadAsset::register($this->view);
        AddressWidgetAsset::register($this->view);
    }
}
