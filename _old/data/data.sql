CREATE TABLE `city` (
  `city_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) unsigned NOT NULL DEFAULT '0',
  `region_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`city_id`),
  KEY `country_id` (`country_id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15789521 DEFAULT CHARSET=utf8


CREATE TABLE `country` (
  `country_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`country_id`),
  KEY `city_id` (`city_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7716094 DEFAULT CHARSET=utf8

CREATE TABLE `region` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `city_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`region_id`),
  KEY `country_id` (`country_id`),
  KEY `city_id` (`city_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15789406 DEFAULT CHARSET=utf8

ALTER TABLE city CHANGE `city_id` `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE country CHANGE `country_id` `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE region CHANGE `region_id` `id` int(11) NOT NULL AUTO_INCREMENT;

rename table city to geoapp_city;
rename table country to geoapp_country;
rename table region to geoapp_region;











CREATE TABLE `site_city_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Наименование',
  `name_gen` varchar(63) DEFAULT NULL COMMENT 'для',
  `name_dat` varchar(63) DEFAULT NULL COMMENT 'кому',
  `name_acc` varchar(63) DEFAULT NULL COMMENT 'в',
  `name_abl` varchar(63) DEFAULT NULL COMMENT 'кем',
  `name_pre` varchar(63) DEFAULT NULL COMMENT 'где',
  `map_center` varchar(63) DEFAULT NULL COMMENT 'Центр на карте',
  `country_id` int(10) unsigned NOT NULL COMMENT 'Страна',
  `name_en` varchar(63) DEFAULT NULL COMMENT 'Сранслит',
  `yandexRegionId` int(10) unsigned DEFAULT NULL COMMENT 'Яндекс id',
  `oblName` varchar(63) DEFAULT NULL COMMENT 'Область',
  `bNoDistricts` int(11) NOT NULL DEFAULT '0' COMMENT '',
  `oblNamePre` varchar(255) DEFAULT NULL COMMENT '',
  `oblNameEn` varchar(63) DEFAULT NULL COMMENT '',
  `bRegionCenter` tinyint(1) NOT NULL DEFAULT '1' COMMENT '',
  `oblNameGen` varchar(63) DEFAULT NULL COMMENT 'в',
  `regionId` int(10) unsigned NOT NULL COMMENT 'Регион, где находится город',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`),
  KEY `country_id` (`country_id`),
  KEY `fkRegionId` (`regionId`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8

ALTER TABLE site_city_list DROP domainRosCatalog;
ALTER TABLE site_city_list DROP imgTop;
ALTER TABLE site_city_list DROP imgLeft;
ALTER TABLE site_city_list DROP domainCatalog;
ALTER TABLE site_city_list DROP domainRasprodaga;

ALTER TABLE site_city_list DROP position;
ALTER TABLE site_city_list DROP domain;
ALTER TABLE site_city_list DROP map_border;
ALTER TABLE site_city_list DROP titleLeft;
ALTER TABLE site_city_list DROP titleTop;
ALTER TABLE site_city_list DROP bRegionCenter;



CREATE TABLE `site_city_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `name_short` varchar(32) DEFAULT NULL,
  `name_in_short` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1',
  `name_en` varchar(255) DEFAULT NULL,
  `name_gen` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3228 DEFAULT CHARSET=utf8



rename table site_city_list to geo_city_list;
rename table site_city_regions to geo_city_regions;
rename table site_country to geo_country;
rename table site_country_regions to geo_country_regions;
rename table site_metro_lines to geo_metro_lines;
rename table site_metro_stations to geo_metro_stations;
rename table site_metro_stations_near to geo_metro_stations_near;

