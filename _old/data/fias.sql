SELECT history
FROM im_order;

SELECT *
FROM d_fias_addrobj
WHERE postalcode = '141570';
SELECT *
FROM geo_addrobj
WHERE aoguid = 'c478711c-30e7-49bd-a276-93d590e60d66';
SELECT *
FROM geo_addrobj
WHERE aoguid = '395203ad-a8a5-4708-944c-790ec93bf8a3';

ALTER TABLE d_fias_addrobj RENAME TO geo_addrobj;


CREATE INDEX geo_addrobj_name ON geo_addrobj (formalname)
  WHERE (aolevel IN (4, 5, 6, 7) AND geo_addrobj.actstatus = 1);

CREATE INDEX geo_addrobj_parentguid ON geo_addrobj (parentguid)
  WHERE (aolevel IN (4, 5, 6, 7) AND geo_addrobj.actstatus = 1);

DROP INDEX geo_addrobj_name;

SELECT
  g.postalcode,
  CONCAT(g.shortname, ' ', g.formalname)   AS pname,
  g.aoguid,
  g.parentguid,
  CONCAT(gp.formalname, ' ', gp.shortname) AS pname,
  gp.formalname                            AS pname
FROM geo_addrobj g
  LEFT JOIN geo_addrobj gp ON g.parentguid = gp.aoguid
WHERE g.formalname ~* 'Менделеево'
      AND g.actstatus = 1
      AND g.aolevel IN (4, 5, 6, 7)
      AND gp.actstatus = 1
      AND gp.aolevel IN (1, 2, 3, 4)
      AND gp.actstatus = 1
      AND gp.actstatus = 1
LIMIT 100;

CREATE OR REPLACE VIEW test_view
AS
  SELECT
    g.aoid,
    g.formalname,
    g.postalcode,
    CONCAT(g.shortname, '. ', g.formalname)             AS name,
    g.aolevel,
    g.aoguid,
    g.parentguid,
    CONCAT(gcity.formalname, '. ', gcity.shortname)     AS gcityname,
    CONCAT(gregion.formalname, '. ', gregion.shortname) AS gregionname,
    gregion.aoguid                                      AS gregionaoid
  FROM geo_addrobj g
    LEFT JOIN geo_addrobj gcity ON g.parentguid = gcity.aoguid
    LEFT JOIN geo_addrobj gregion ON gcity.parentguid = gregion.aoguid
  WHERE g.actstatus = 1
        AND g.aolevel IN (6, 7)
  ORDER BY gregion.formalname, gcity.formalname, g.postalcode;


SELECT
  g.aoid,
  g.formalname,
  g.postalcode,
  CONCAT(g.shortname, '. ', g.formalname)             AS NAME,
  g.aolevel,
  g.aoguid,
  g.parentguid,
  CONCAT(gcity.formalname, '. ', gcity.shortname)     AS gcityname,
  CONCAT(gregion.formalname, '. ', gregion.shortname) AS gregionname
FROM geo_addrobj g
  LEFT JOIN geo_addrobj gcity ON g.parentguid = gcity.aoguid
  LEFT JOIN geo_addrobj gregion ON gcity.parentguid = gregion.aoguid
WHERE g.actstatus = 1
      AND g.aolevel IN (6, 7);


EXPLAIN ( ANALYZE ) SELECT
                      aoid,
                      postalcode,
                      name,
                      aolevel,
                      aoguid,
                      parentguid,
                      gcityname,
                      gregionname
                    FROM test_view
                    WHERE formalname ILIKE 'Институтск%'
                    LIMIT 200;

CREATE INDEX geo_addrobj_aoguid ON geo_addrobj (aoguid);

WITH RECURSIVE child_to_parents AS (
  SELECT geo_addrobj.*
  FROM geo_addrobj
  WHERE aoid = '395203ad-a8a5-4708-944c-790ec93bf8a3'
  UNION ALL
  SELECT geo_addrobj.*
  FROM geo_addrobj, child_to_parents
  WHERE geo_addrobj.aoguid = child_to_parents.parentguid
        AND geo_addrobj.currstatus = 0
)
SELECT *
FROM child_to_parents
ORDER BY aolevel;


-- сбор всех parentguid
WITH all_parents AS (
    SELECT DISTINCT parentguid
    FROM geo_addrobj
    WHERE (currstatus = 0) AND (parentguid IS NOT NULL)
)
-- выбор элементов самого нижнего уровня (не входящих в список all_parents)
-- и получение полного адреса
SELECT *
FROM all_parents
WHERE (curstatus = 0) AND (aoguid NOT IN (SELECT parentguid
                                          FROM all_parents))


CREATE INDEX trgm_idx ON geo_addrobj USING GIN (formalname gin_trgm_ops);


UPDATE geo_addrobj
SET
  parentname       = b.gcityname,
  parentparentguid = b.gregionaoid,
  parentparentname = b.gregionname
FROM test_view AS b
WHERE b.aoid = geo_addrobj.aoid;


ALTER TABLE geo_addrobj RENAME COLUMN parentparentguid TO parentparentaoid;