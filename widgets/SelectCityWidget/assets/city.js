var formatCitySelect = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }
    var markup =
        '<div class="row">' +
        '<div class="col-sm-6">' +
        repo.text +
        '</div>' +
        '<div class="col-sm-6 parent-string">' +
        repo.parent_string +
        '</div>' +
        '</div>';

    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.text || repo.name;
}

var setUserCity = function (event) {
    $('#user-select-city').val($(event.currentTarget).val());
}
