<?php
namespace emilasp\geoapp\widgets\SelectCityWidget;

use emilasp\geoapp\models\GeoKladr;
use emilasp\geoapp\models\Kladr;
use kartik\typeahead\TypeaheadAsset;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;

/**
 * Автокомплит адрес
 *
 * Class SelectCityWidget
 * @package emilasp\geoapp\widgets\SelectCityWidget
 */
class SelectCityWidget extends InputWidget
{
    public $name = 'select-city-input';
    public $model;
    public $attribute;
    public $label;

    public $countMillions = 10;


    public function init()
    {
        parent::init();
        $this->registerAssets();

        $this->setLabel();
    }

    public function run()
    {
        echo $this->render('select-city', [
            'id'             => $this->id,
            'label'          => $this->label,
            'value'          => $this->value,
            'citiesMillions' => $this->getCitiesMillions(),
            'urlToSaveCity'  => $this->getUrlCitySelect(),
        ]);
        if ($this->model) {
            echo Html::activeHiddenInput($this->model, $this->attribute, ['id' => 'user-select-city']);
        } else {
            echo Html::hiddenInput($this->name, $this->value, ['id' => 'user-select-city']);
        }
    }

    /** Получаем url до контроллера
     * @return string
     */
    protected function getUrlCitySelect()
    {
        return Url::toRoute('/geo/kladr/select-city');
    }
    
    /**
     * Устанавливаем имя города
     */
    protected function setLabel()
    {
        if ($this->model) {
            $attribute = preg_replace('/(\[\w\]+)/', '', $this->attribute);
            $this->value = $this->model->{$attribute};
        }

        if (!$this->value) {
            $this->label = Yii::t('site', '-select-');
        } else {
            $model       = GeoKladr::find()->where(['id' => $this->value])->one();
            $this->label = $model->name;
        }
    }

    /** Получаем города миллионики
     * @return array|yii\db\ActiveRecord[]
     */
    protected function getCitiesMillions()
    {
        return GeoKladr::find()->where(['actual' => '00'])->andWhere([
            '>',
            'count',
            0,
        ])->orderBy('count DESC')->limit($this->countMillions)->all();
    }


    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        SelectCityWidgetAsset::register($this->view);

        $title = Yii::t('im', 'Please select you city');

        $js = <<<JS
        $('.select-city-label').jBox('Modal', {
            title: '{$title}',
            content: $('.select-city-modal')
        });
JS;
        $this->view->registerJs($js);
    }
}
