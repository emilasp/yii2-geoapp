<?php
namespace emilasp\geoapp\widgets\SelectCityWidget;

use emilasp\geoapp\models\GeoKladr;
use emilasp\geoapp\models\Kladr;
use kartik\typeahead\TypeaheadAsset;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;
use yii\widgets\InputWidget;

/**
 * Автокомплит адрес
 *
 * Class SelectCityUserWidget
 * @package emilasp\geoapp\widgets\SelectCityWidget
 */
class SelectCityUserWidget extends SelectCityWidget
{
    const COOKIE_KEY = 'city';

    public $model;
    public $attribute;
    public $label;

    public function init()
    {
        parent::init();
        $this->registerAssets();

        $this->setUserCity();

        $this->setLabel();
    }

    /** Получаем url до контроллера
     * @return string
     */
    protected function getUrlCitySelect()
    {
        return Url::toRoute('/geo/kladr/select-city-user');
    }

    /**
     *  Устанавливаем город для пользователя в куку
     */
    private function setUserCity()
    {
        if (Yii::$app->request->isPjax && Yii::$app->request->isPost) {
            $this->value = Yii::$app->request->post('value');
            Yii::$app->response->cookies->add(new Cookie([
                'name'  => self::COOKIE_KEY,
                'value' => $this->value,
            ]));
        } else {
            $this->value = Yii::$app->request->cookies->getValue(self::COOKIE_KEY, '');
        }
    }

    /**
     * Registers the needed assets
     */
    private function registerAssets()
    {
        SelectCityWidgetAsset::register($this->view);

        $title = Yii::t('site', 'Please select you city');

        $js = <<<JS
        
JS;
        $this->view->registerJs($js);

    }
}
