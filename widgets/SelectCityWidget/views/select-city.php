<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
?>

<?php Pjax::begin(['id' => 'pjax-select-city']) ?>
<span class="select-city">
    <span class="select-city-label"><?= $label ?></span>
    <div class="select-city-modal" style="display: none">
        <div class="cities-millions city-column">
            <?php foreach ($citiesMillions as $city) : ?>
                <div class="city-name ">
                    <label class="<?= $city->id == $value ? 'city-selected' : '' ?>">
                        <?= Html::radio('city-millions[]', $value == $city->id, [
                            'value' => $city->id,
                        ]) ?>
                        <?= $city->name ?>
                    </label>
                </div>
            <?php endforeach ?>
        </div>

        <div class="select-city-other">
            <h3><?= Yii::t('site', 'Select city other') ?>:</h3>

            <?php
            $resultsJs = <<< JS
function (data, params) {
console.log(data);
    params.page = params.page || 1;
    return {
        results: data.results,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;
            ?>

            <?= Select2::widget([
                'id'            => 'geo-city-select',
                'language'      => 'ru',
                'name'          => 'geo-city-select',
                'value'         => '',
                'initValueText' => '',
                'options'       => ['placeholder' => Yii::t('site', 'Search city')],
                'pluginOptions' => [
                    'allowClear'         => true,
                    'minimumInputLength' => 1,
                    'ajax'               => [
                        'url'            => Url::toRoute('/geo/kladr/search-city'),
                        'dataType'       => 'json',
                        'delay'          => 250,
                        'data'           => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                        'processResults' => new JsExpression($resultsJs),
                        'cache'          => true,
                    ],
                    'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                    'templateResult'     => new JsExpression('formatCitySelect'),
                ],
                'pluginEvents'  => [
                    'change' => 'function(event) { setUserCity(event); }',
                ],
            ]) ?>
        </div>

        <div class="text-right">
            <button type="button" class="btn btn-success btn-change-city">
                <?= Yii::t('site', 'Save') ?>
            </button>
        </div>

    </div>



</span>
<?php Pjax::end() ?>

<?php
$title = Yii::t('site', 'Please select you city');

$js = <<<JS

        $('.select-city-label').jBox('Modal', {
            title: '{$title}',
            content: $('.select-city-modal')
        });

    $('.cities-millions input:checked').closest('label').addClass('city-selected');

    $('body').on('change', '.cities-millions input', function () {
        var input = $(this);
        var label = input.closest('label');
        var isChecked = $(this).is(':checked');
        var inputs = input.closest('.cities-millions').find('input');

        label.closest('.cities-millions').find('label').removeClass('city-selected');
        
        if (isChecked) {
            label.addClass('city-selected');
            $('#user-select-city').val(input.val())
        }
    });

    $('body').on('click', '.btn-change-city', function () {
        var value = $('#user-select-city').val();
        var name = $('#user-select-city').attr('name');
        $.pjax({
            container: "#pjax-select-city",
            method: "POST",
            timeout : 0,
            url: "{$urlToSaveCity}",
            data: {value:value, name:name},
            push:false
        });
        $('.jBox-container').remove();
        $('.jBox-closeButton').click();
    });
    
    $('body').on("pjax:end", "#pjax-select-city", function() {
        $('.select-city-label').jBox('Modal', {
            title: '{$title}',
            content: $('.select-city-modal')
        });   
    });
JS;
$this->registerJs($js);