<?php
namespace emilasp\geoapp\widgets\SelectCityWidget;

use yii\web\AssetBundle;

/**
 * Class SelectCityWidgetAsset
 * @package emilasp\im\common\widgets\SelectCityWidget
 */
class SelectCityWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $js  = ['city.js'];
    public $css = ['city.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
