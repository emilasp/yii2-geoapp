<?php
use kartik\widgets\Typeahead;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

?>

<div class="address" id="<?= $id ?>">
    <div class="row">
        <div class="col-xs-3">
            <?= AutoComplete::widget([
                'name'          => 'index',
                'options'       => [
                    'id' => 'index-' . $id,
                ],
                'clientOptions' => [
                    'source' => ['USA', 'RUS'],
                ],
            ]); ?>
        </div>
        <div class="col-xs-9">
            <?php
            $template = '<div><p class="repo-language">{{index}}</p>' .
                        '<p class="repo-name">{{name}}</p>' .
                        '<p class="repo-description">{{name}}</p></div>';
            ?>
            <?=
            Typeahead::widget([
                'name' => 'sports',
                'options' => ['placeholder' => 'Filter as you type ...'],
                'pluginOptions' => ['highlight'=>true],
                'dataset' => [
                    [
                        'limit' => 30,
                        //'prefetch' => Url::toRoute('/geo/kladr/get-city'),
                        'remote' => [
                            'url' => Url::toRoute('/geo/kladr/get-city') . '?q=%QUERY',
                            'wildcard' => '%QUERY'
                        ],
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                        'display' => 'value',
                        'templates' => [
                            'suggestion' => new JsExpression("Handlebars.compile('{$template}')"),
                            //'header' => '<h3 class="league-name">NBA Teams</h3>'
                        ]
                    ],
                ]
            ]);
            ?>
            
            
            <?= AutoComplete::widget([
                'name'          => 'city',
                'options'       => [
                    'id' => 'city-' . $id,
                ],
                'clientOptions' => [
                    'source'      => Url::toRoute('/geo/kladr/get-city'),
                    'dataType'    => 'json',
                    'autoFill'    => true,
                    'minLength'   => '3',
                    '_renderItem' => new JsExpression('function( ul, item ) {
            item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");

                        return $( "<li></li>" )
                            .data( "item.autocomplete", item )
                            .append( "<a>" + item.label + " / " + item.blok_num + "</a>" + ui.item.id)
                            .appendTo( ul );
                        }'),

                    'success'     => new JsExpression("function (data) {
                        response($.map(data, function (item) {
                            return item.name;
                        }))
                    }"),
                ],
            ]); ?>
        </div>
    </div>

</div>