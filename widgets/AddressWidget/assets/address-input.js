$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};

(function ($) {
    $.fn.fias = function (_params) {
        // при многократном вызове функции настройки будут сохранятся
        var params = $(this).extend({
            'id': 'address-tt',
            'len': 2,
            'zip': 1,
            'debug': 0
        }, _params);

        var make = function () {
            var input = $(this);

            if (!params.debug) {
                input.css('display', 'none');
            }

            var html = '<div class="row" id="block-' + input.attr('id') + '">'
                + '<div class="col-md-3">'
                + '<label>Индекс</label>'
                + '<input type="text" class="fias-zip form-control" autocomplete="off" spellcheck="false">'
                + '</div>'
                + '<div class="col-md-9">'
                + '<label>Улица/н.п.</label>'
                + '<input type="text" class="fias-address form-control typeahead tt-query" autocomplete="off" spellcheck="false" >'
                + '</div>'
                + '</div>'
            $(html).insertAfter(input);

            var block = $('#block-' + input.attr('id'));
            var zip = block.find('.fias-zip');
            var address = block.find('.fias-address');

            zip.bind("change keyup", function () {
                if (this.value.match(/[^0-9]/g)) {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }

                if (this.value.length >= 6) {
                    this.value = this.value.substring(0, 6);
                    address.setCursorPosition(address.val().length);
                }

                if (this.value.length == 6) {

                    var val_street = address.val();
                    address.val(this.value + ', ' + val_street.replace(/(\d{6}, )/gm, ''));
                }
            });

            var sources = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('aoid'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {url: "/geo/fias/address?search=%QUERY", wildcard: "%QUERY"}
            });

            $(address).typeahead({
                minLength: 2,
                highlight: true,
                hint:false
            }, {
                name: 'best-pictures',
                display: 'formalname',
                limit: "200",
                source: sources
            });

            $(address).typeahead('val', input.data('address'));
            $(zip).val(input.data('index'));

            $(address).bind('typeahead:select', function(ev, suggestion) {
                console.log(suggestion);
                input.val(suggestion.aoid);
                input.data('aoid', suggestion.aoid);
                input.data('addess', suggestion.formalname);
                input.change();
                updateAddress(suggestion);
            });
        };

        return this.each(make);
    };
})(jQuery);

