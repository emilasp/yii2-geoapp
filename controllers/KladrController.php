<?php

namespace emilasp\geoapp\controllers;

use emilasp\geoapp\models\GeoAddrobj;
use emilasp\geoapp\models\GeoKladr;
use emilasp\geoapp\models\GeoStreet;
use emilasp\geoapp\models\Kladr;
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityUserWidget;
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityWidget;
use Yii;
use emilasp\core\components\base\Controller;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * KladrController
 */
class KladrController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['address', 'get-city'],
                'rules'        => [
                    [
                        'actions' => ['address', 'get-city'],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionSearchCity()
    {
        $results = [];
        $row     = [];
        $term    = Yii::$app->request->get('q');
        if ($term) {
            $query = GeoKladr::find()->where(['actual' => '00', 'is_city' => 1])
                ->andWhere(['ILIKE', 'name', $term]);
            
            $items = $query->orderBy(['level' => 'DESC', 'count' => 'DESC'])->limit(30)->all();

            foreach ($items as $item) {
                $row['id']            = $item->id;
                $row['text']          = $item->name_full;
                $row['parent_string'] = $item->parent_string ? $item->parent_string : '';
                array_push($results, $row);
            }
        } else {
            $row['id']   = 0;
            $row['name'] = '....';
            array_push($results, $row);
        }
        echo json_encode(['results' => $results]);
    }
    
    /**
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionSearchStreet()
    {
        $results = [];
        $row     = [];
        $term    = Yii::$app->request->get('q');
        if ($term) {
            $query = GeoStreet::find()->where(['ILIKE', 'name', $term]);
            $items = $query->orderBy(['name' => 'asc'])->limit(30)->all();

            foreach ($items as $item) {
                $row['id']            = $item->id;
                $row['text']          = $item->name;
                $row['parent_string'] = '';
                array_push($results, $row);
            }
        } else {
            $row['id']   = 0;
            $row['name'] = '....';
            array_push($results, $row);
        }
        echo json_encode(['results' => $results]);
    }

    /**
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetStreet()
    {
        $results = [];
        $row     = [];
        $term    = Yii::$app->request->get('q');
        if ($term) {
            $query = GeoStreet::find()->where(['ILIKE', 'name', $term]);
            $items = $query->orderBy(['name' => 'asc'])->limit(30)->all();

            foreach ($items as $item) {
                $row['id']            = $item->id;
                $row['text']          = $item->name;
                $row['parent_string'] = '';
                array_push($results, $row);
            }
        } else {
            $row['id']   = 0;
            $row['name'] = '....';
            array_push($results, $row);
        }
        echo json_encode(['results' => $results]);
    }
    
    /** Пользовательскийц выбор города
     * @return string
     * @throws \Exception
     */
    public function actionSelectCity()
    {
        if (Yii::$app->request->isPjax && Yii::$app->request->isPost) {
            return SelectCityWidget::widget([
                'name'  => Yii::$app->request->post('name'),
                'value' => Yii::$app->request->post('value'),
            ]);
        }
    }

    /** Пользовательскийц выбор города
     * @return string
     * @throws \Exception
     */
    public function actionSelectCityUser()
    {
        if (Yii::$app->request->isPjax && Yii::$app->request->isPost) {
            return SelectCityUserWidget::widget([]);
        }
    }


    private function getPostalCode($search)
    {
        preg_match('/^(\d{6})/i', $search, $m);
        if (isset($m[0]) && strlen($m[0]) === 6) {
            return $m[0];
        }
        return false;
    }

    /**
     * Lists all Variety models.
     * @return mixed
     */
    public function actionAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $search = Yii::$app->request->get('search');
        $index  = $this->getPostalCode($search);

        if ($index) {
            $search = str_replace($index, '', $search);
            $search = trim($search, ', ');
        }
        $data = GeoAddrobj::getObjectsByName($search, $index);

        $data = ArrayHelper::map($data, 'aoid', 'formalname');
        $out  = [];
        foreach ($data as $index => $formalname) {
            $out[] = ['aoid' => $index, 'formalname' => $formalname,];
        }
        return $out;
    }


}
